# README #

### What is this repository for? ###

[QA Engineering Problem](https://gist.github.com/sturnquest/308557fb46d0b36cb49c) Solution 

Language and tools used: Python 2.7, WebDriver, [unittest](https://docs.python.org/2/library/unittest.html) framework, [ddt](https://pypi.python.org/pypi/ddt) framework for test cases parametrization.

Those are automated test cases to verify that state tax was applied correctly and total sum was adjusted correspondingly (assuming that subtotal calculations are verified more extensively on corresponding regression test suite).

Note: State tax for North Dakota currently is 10% (according to task it should be 5%). So this particular case fails due to issue.

As for opportunities to modify each page's html to make it more testable. Some elements does not have unique ids, I would suggest to add them as it’s the best way to find element in WebDriver. For example,  Checkout button on Welcome page was defined by Xpath. Having access to application code, it’s better to add ids along with test or work with developers on it.

To run tests from Terminal: download project, navigate to project directory (cd), run ./taxes_test.py 
 