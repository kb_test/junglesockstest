#!/usr/bin/env python

import unittest

from ddt import data
from ddt import ddt
from ddt import unpack
from selenium import webdriver

from pages import welcome_page
from pages import confirmation_page


WELCOME_PAGE_URL = "https://jungle-socks.herokuapp.com/"
ERROR_MESSAGE = "We're sorry, but something went wrong."


class BasePageTestCase(unittest.TestCase):
    """Superclass for concrete test cases."""

    def setUp(self):
        """Loads a browser driver."""
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()

    def tearDown(self):
        """Performs clean up."""
        self.driver.quit()


@ddt
class TaxesTestCase(BasePageTestCase):
    """Tests state taxes calculations."""

    def setUp(self):
        """Loads welcome page and creates page object."""
        BasePageTestCase.setUp(self)
        self.driver.get(WELCOME_PAGE_URL)
        self.welcome_page = welcome_page.WelcomePage(self.driver)


    @data(('California', '$18.88', '$254.88'), ('Alaska', '$11.80', '$247.80'),
          ('New York', '$14.16', '$250.16'), ('Minnesota', '$0.00', '$236.00'))
    @unpack
    def testTaxes(self, state, expected_taxes, expected_total):
        """Tests taxes and total sum calculation functionality."""
        self.welcome_page.enter_quantities(zebra_quantity=2,
                                           lion_quantity=1,
                                           elephant_quantity=3,
                                           giraffe_quantity=5)
        self.welcome_page.select_state(state)
        self.welcome_page.click_checkout()

        self.confirmation_page = confirmation_page.ConfirmationPage(self.driver)
        confirmation_page.ConfirmationPage.wait_for_load(self.driver)

        self.assertEqual('$236.00', self.confirmation_page.get_subtotal())
        self.assertEqual(expected_taxes, self.confirmation_page.get_taxes())
        self.assertEqual(expected_total, self.confirmation_page.get_total())

    @data(('California', '$241.20', '$3,256.20'),
          ('Florida', '$150.75', '$3,165.75'),
          ('New York', '$180.90', '$3,195.90'),
          ('Minnesota', '$0.00', '$3,015.00'))
    @unpack
    def testTaxesNontrivial(self, state, expected_taxes, expected_total):
        """Tests taxes and total sum calculation functionality with logically
         correct (positive), 0 and negative number of items ordered."""
        # Note: this test does not verify inventory count. Assuming, this is
        # tested separately.
        self.welcome_page.enter_quantities(zebra_quantity=100,
                                           lion_quantity=0,
                                           elephant_quantity=49,
                                           giraffe_quantity=-1)
        self.welcome_page.select_state(state)
        self.welcome_page.click_checkout()

        self.confirmation_page = confirmation_page.ConfirmationPage(self.driver)
        confirmation_page.ConfirmationPage.wait_for_load(self.driver)

        self.assertEqual('$3,015.00', self.confirmation_page.get_subtotal())
        self.assertEqual(expected_taxes, self.confirmation_page.get_taxes())
        self.assertEqual(expected_total, self.confirmation_page.get_total())

    @data(('Alabama',), ('Alaska',), ('Arizona',), ('Arkansas',), ('Colorado',),
          ('Connecticut',), ('Delaware',), ('Florida',), ('Georgia',),
          ('Hawaii',), ('Idaho',), ('Illinois',), ('Indiana',), ('Iowa',),
          ('Kansas',), ('Kentucky',), ('Louisiana',), ('Maine',), ('Maryland',),
          ('Massachusetts',), ('Michigan',), ('Mississippi',), ('Missouri',),
          ('Montana',), ('Nebraska',), ('Nevada',), ('New Hampshire',),
          ('New Jersey',), ('New Mexico',), ('North Carolina', ),
          ('North Dakota',), ('Ohio',), ('Oklahoma',), ('Oregon',),
          ('Pennsylvania',), ('Rhode Island',), ('South Carolina',),
          ('South Dakota',), ('Tennessee',), ('Texas',), ('Utah',),
          ('Vermont',), ('Virginia',), ('Washington',), ('West Virginia',),
          ('Wisconsin',), ('Wyoming', ))
    @unpack
    def testDefaultTaxes(self, state):
        """Tests taxes and total sum calculation for all states with default
        5% tax"""
        self.welcome_page.enter_quantities(zebra_quantity=4,
                                           lion_quantity=6,
                                           elephant_quantity=10,
                                           giraffe_quantity=1)
        self.welcome_page.select_state(state)
        self.welcome_page.click_checkout()

        self.confirmation_page = confirmation_page.ConfirmationPage(self.driver)
        confirmation_page.ConfirmationPage.wait_for_load(self.driver)

        self.assertEqual('$539.00', self.confirmation_page.get_subtotal())
        self.assertEqual('$26.95', self.confirmation_page.get_taxes())
        self.assertEqual('$565.95', self.confirmation_page.get_total())

    def testTaxesStateNotSelected(self):
        """Tests that error is displayed instead of taxes and total sum
         calculations in case if state was not specified."""
        self.welcome_page.enter_quantities(zebra_quantity=2,
                                           lion_quantity=1,
                                           elephant_quantity=3,
                                           giraffe_quantity=-5)
        self.welcome_page.select_state('')
        self.welcome_page.click_checkout()

        self.assertIn(ERROR_MESSAGE, self.driver.page_source)


if __name__ == '__main__':
    unittest.main()
