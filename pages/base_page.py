#!/usr/bin/env python

class BasePage(object):
    """Superclass for concrete pages."""

    def __init__(self, driver):
        """Initializes the page with given driver."""
        self.driver = driver