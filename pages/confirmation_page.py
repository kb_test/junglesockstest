#!/usr/bin/env python

import base_page

from selenium.webdriver.common import by
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support import ui


class ConfirmationPage(base_page.BasePage):
    """Page object representing confirmation page."""

    def __init__(self, driver):
        """Initializes the page with given driver."""
        base_page.BasePage.__init__(self, driver)
        self.wait_for_load(driver)

        self.subtotal_text = driver.find_element_by_id('subtotal')
        self.taxes_text = driver.find_element_by_id('taxes')
        self.total_text = driver.find_element_by_id('total')

    @classmethod
    def wait_for_load(cls, driver):
        """Waits until the page is loaded."""
        ui.WebDriverWait(driver, 10).until(
            expected_conditions.visibility_of_element_located((
                by.By.ID, 'subtotal')))

    def get_subtotal(self):
        """Returns order subtotal."""
        return self.subtotal_text.text

    def get_taxes(self):
        """Returns order taxes."""
        return self.taxes_text.text

    def get_total(self):
        """Returns order total (subtotal plus taxes)."""
        return self.total_text.text
