#!/usr/bin/env python

import base_page

from selenium.webdriver.common import by
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support import ui


class WelcomePage(base_page.BasePage):
    """Page object representing JungleSocks welcome page."""

    def __init__(self, driver):
        """Initializes the page with given driver."""

        base_page.BasePage.__init__(self, driver)
        self.wait_for_load(driver)

        self.zebra_quantity_textfield = driver.\
            find_element_by_id('line_item_quantity_zebra')

        self.lion_quantity_textfield = driver.\
            find_element_by_id('line_item_quantity_lion')

        self.elephant_quantity_textfield = driver.\
            find_element_by_id('line_item_quantity_elephant')

        self.giraffe_quantity_textfield = driver.\
            find_element_by_id('line_item_quantity_giraffe')

        self.checkout_button = driver.\
            find_element_by_xpath("//input[@type='submit']")

    @classmethod
    def wait_for_load(cls, driver):
        """Waits until the page is loaded."""
        ui.WebDriverWait(driver, 10).until(
            expected_conditions.visibility_of_element_located((
                by.By.ID, 'line_item_quantity_zebra')))

    def enter_quantities(self,
                         zebra_quantity,
                         lion_quantity,
                         elephant_quantity,
                         giraffe_quantity):
        """Enters number of items of each type to order."""
        self.zebra_quantity_textfield.send_keys(zebra_quantity)
        self.lion_quantity_textfield.send_keys(lion_quantity)
        self.elephant_quantity_textfield.send_keys(elephant_quantity)
        self.giraffe_quantity_textfield.send_keys(giraffe_quantity)

    def select_state(self, state):
        """Selects state to ship."""
        select_item = ui.Select(
            self.driver.find_element_by_xpath("//select[@name='state']"))
        select_item.select_by_visible_text(state)

    def click_checkout(self):
        """Clicks Checkout button."""
        self.checkout_button.click()
